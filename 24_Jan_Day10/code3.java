//GFG -Product array puzzle

class Solution 
{ 
	public static long[] productExceptSelf(int nums[], int n) 
	{ 
        long mult=1;
        int flag=0;
        long num[]=new long[n];
        for(int i=0;i<n;i++){
            if(nums[i]==0){
                flag++;
                continue;
            }
            mult=mult*nums[i];
        }
        for(int i=0;i<n;i++){
            if(nums[i]!=0){
                if(flag>0)
                    num[i]=0;
                else
                    num[i]=mult/nums[i];
            }
            else {
                if(flag==1)
                    num[i]=mult;
                else
                    num[i]=0;
            }
        }
        return num;
	} 
    public static void main(String[] args) {
        int nums[]={10, 3, 5, 6, 2};
        long num[]=productExceptSelf(nums, nums.length);
        for(long e:num){
            System.out.println(e);
        }
    }
} 