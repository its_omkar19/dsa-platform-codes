// Leetcode 14 - Longest Common Prefix

import java.util.Arrays;

class Solution {
    public static String longestCommonPrefix(String[] strs) {
        Arrays.sort(strs);
        String str1=strs[0];
        String str2=strs[strs.length-1];
        int count=0;
        for(int i=0;i<str1.length();i++){
            if(str2.charAt(i)==str1.charAt(i)){
                count++;
            }
            else{
                break;
            }
        }
        if(count==0)
            return "";
        return str1.substring(0,count);
    }
    public static void main(String[] args) {
        String strs[]={"flower","flow","flight"};
        System.out.println(longestCommonPrefix(strs));
    }
}