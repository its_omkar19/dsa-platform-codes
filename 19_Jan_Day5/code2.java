//GFG - Equal Left and Right Subarray Sum

class Solution{
	static int equalSum(int [] A, int N) {
		 int sum2=0;
        for(int i:A)
            sum2=sum2+i;
        int sum1=0;
        for(int i=0;i<A.length;i++){
            
            sum2=sum2-A[i];
            if(sum1==sum2)
                return i+1;
            sum1=sum1+A[i];
        }
        return -1;
	}
    public static void main(String[] args) {
        int A[]={1,3,5,2,2};
        System.out.println(equalSum(A,A.length));
    }
}