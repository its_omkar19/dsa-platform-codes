//GFG - Equilibrium Point


class Solution {
    public static int equilibriumPoint(long arr[], int n) {
        long sum2=0;
        for(long i:arr)
            sum2=sum2+i;
        long sum1=0;
        for(int i=0;i<arr.length;i++){
            
            sum2=sum2-arr[i];
            if(sum1==sum2)
                return i+1;
            sum1=sum1+arr[i];
        }
        return -1;
    }
    public static void main(String[] args) {
        long arr[]={1,3,5,2,2};
        System.out.println(equilibriumPoint(arr,arr.length));
    }
}