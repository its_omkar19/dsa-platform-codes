//Leetcode 724 : Find Pivot Index
//sum of right side array and left side array is equal

class Solution {
    public static int pivotIndex(int[] nums) {
        int sum2=0;
        for(int i:nums)
            sum2=sum2+i;
        int sum1=0;
        for(int i=0;i<nums.length;i++){
            
            sum2=sum2-nums[i];
            if(sum1==sum2)
                return i;
            sum1=sum1+nums[i];
        }
        return -1;
    }
    public static void main(String[] args) {
        int nums[]={1,7,3,6,5,6};
        System.out.println(pivotIndex(nums));       
    }
}