// GFG - Transpose of Matrix

class Main
{
    public static void transpose(int n,int a[][])
    {
        for(int i=0;i<n;i++){
            for(int j=0;j<i;j++){
                a[i][j]=a[i][j]+a[j][i];
                a[j][i]=a[i][j]-a[j][i];
                a[i][j]=a[i][j]-a[j][i];
            }
        }
        for(int arr[]:a){
            for(int x:arr)
                System.out.println(x);
        }
    }
    public static void main(String[] args) {
        int a[][]={{1,2,3},{1,2,3},{1,2,3}};
        for(int arr[]:a){
            for(int x:arr)
                System.out.println(x);
        }
        transpose(a.length, a);
    }
}