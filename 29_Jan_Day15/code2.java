// GFG - Bubble Sort

class Solution
{
	public static void bubbleSort(int arr[], int n)
    {
        
        boolean swapped;
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    arr[j]=arr[j]+arr[j+1];
                    arr[j+1]=arr[j]-arr[j+1];
                    arr[j]=arr[j]-arr[j+1];
                    swapped = true;
                }
            }
            if (swapped == false)
                break;
        }
            for(int x:arr)
                System.out.println(x);
        
    }
    public static void main(String[] args) {
        int arr[]={4,3,1,2};
        bubbleSort(arr, arr.length);
    }
}