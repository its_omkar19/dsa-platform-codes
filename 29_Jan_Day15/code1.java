//Leetcode 118 - Pascal's Triangle

import java.util.*;

class Solution {
    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> pt = new ArrayList();
        for(int i=1;i<=numRows;i++){
            List<Integer> row=new ArrayList();
            for(int j=0;j<i;j++){
                if(j==0 || j==i-1)
                    row.add(1);
                else{
                    row.add(pt.get(i-2).get(j)+pt.get(i-2).get(j-1));
                }
            }
            pt.add(row);
        }
        return pt;
    }
    public static void main(String[] args) {
        List<List<Integer>> pList=generate(5);
        System.out.println(pList);
    }
}