//Leetcode 136 - single number

class Solution {
    public static int singleNumber(int[] nums) {
        int single=nums[0];
       for(int i=1;i<nums.length;i++){
           single=single ^ nums[i];
       } 
       return single;
    }
    public static void  main(String args[]){
        int arr[]=new int[]{2,5,3,4,3,4,2,};
        System.out.println(singleNumber(arr));
    }
}