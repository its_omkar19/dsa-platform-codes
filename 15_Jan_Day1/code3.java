// GFG - Key Pair

import java.util.Arrays;

class Solution {
    static boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        Arrays.sort(arr);
        int start=0;
        int end=n-1;
        while(start<end){
            int sum=arr[start]+arr[end];
            if(sum==x)
                return true;
            if(sum>x)
                end--;
            else
                start++;
        }
        return false;
    }
    public static void main(String[] args) {
        int arr[]=new int[]{2,4,5,1,8,6};
        
        System.out.println(hasArrayTwoCandidates(arr,arr.length,14));
    }
}