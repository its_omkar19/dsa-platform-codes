//GFG - First Repeating Element

import java.util.HashMap;

public class code2 {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        HashMap<Integer,Integer> hm=new HashMap<>();
        int num=Integer.MAX_VALUE;
        for(int i=0;i<n;i++){
            if(!hm.containsKey(arr[i]))
                hm.put(arr[i],i);
            else{
                int j=hm.get(arr[i]);
                if(j<num)
                    num=j;
            }
        }
        if(num==Integer.MAX_VALUE)
            return -1;
            
        return num+1;
    }
    public static void main(String[] args) {
        int arr[]=new int[]{1,5,3,4,3,5,6};
        System.out.println(firstRepeated(arr,arr.length));
        }
    }
