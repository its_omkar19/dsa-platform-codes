//Leetcode 2460 - Apply Operations to an Array

class Solution {
    public static void applyOperations(int[] nums) {
        for(int i=0;i<nums.length-1;i++){
            if(nums[i]==nums[i+1]){
                nums[i]=nums[i]*2;
                nums[i+1]=0;
            }
        }
        int j=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]!=0){
                nums[j]=nums[i];
                j++;
            }
        }
        while(j<nums.length){
            nums[j]=0;
            j++;
        }
    }
    public static void main(String[] args) {
        int nums[]={1,2,2,1,1,0};
        applyOperations(nums);
        for(int x:nums)
            System.out.println(x);
    }
}