// Leetcode 268 & GFG - Missing Element

class Solution {
    static int missingNumber(int array[], int n) {
        int sum=0;
        for(int x:array)
            sum=sum+x;
            
        return (n*(n+1)/2)-sum;
    }
    public static void main(String[] args) {
        int array[]={1,0,2,3,5};
        System.out.println(missingNumber(array,array.length));
    }
}