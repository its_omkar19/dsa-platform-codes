//GFG - Buildings receiving sunlight

class Solution {

    public static int longest(int arr[],int n)
    {   
        int max=Integer.MIN_VALUE;
        int count=0;
        for(int i=0;i<n;i++){
            if(max<=arr[i]){
                max=arr[i];
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int arr[]={6, 2, 8, 4, 11, 13};
        System.out.println(longest(arr, arr.length));
    }
}
