//GFG Leading an array

import java.util.ArrayList;
import java.util.Collections;

class Solution{
    //Function to find the leaders in the array.
    static ArrayList<Integer> leaders(int arr[], int n){
        
        ArrayList<Integer> al=new ArrayList<>();
        int temp=Integer.MIN_VALUE;
        for(int i=n-1;i>=0;i--){
            if(arr[i]>=temp){
                al.add(arr[i]);
                temp=arr[i];
            }
        }
        Collections.reverse(al);
        
        return al;
    }
    public static void main(String[] args) {
        int arr[]={1,2,3,4,0};
        System.out.println(leaders(arr,arr.length));
    }
}