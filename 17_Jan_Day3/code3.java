// GFG - Count pairs with given sum

import java.util.HashMap;

class Solution {
    static int getPairsCount(int[] arr, int n, int k) {
        HashMap<Integer,Integer> hm=new HashMap<>();
        int res=0;
        for(int i=0;i<n;i++){
            int rem=k-arr[i];
            res += hm.getOrDefault(rem,0);
            hm.put(arr[i],hm.getOrDefault(arr[i],0)+1);
            
        }
        return res;
    }
    public static void main(String[] args) {
        int arr[]={1, 5, 7, 1};
        System.out.println(getPairsCount(arr,arr.length,6));
    }
}