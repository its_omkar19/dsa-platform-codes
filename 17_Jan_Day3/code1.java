// Leetcode 169 - Majority Element

class Solution {
    public static int majorityElement(int[] nums) {
        int temp=0;
        int count=0;
        for(int i=0;i<nums.length;i++){
            if(count==0)
                temp=nums[i];
            if(temp==nums[i])
                count++;
            else
                count--;
        }
        return temp;
    }

    //GFG
    static int majorityElement_1(int a[], int size)
    {
        int temp=0;
        int count=0;
        for(int i=0;i<size;i++){
            if(count==0)
                temp=a[i];
            if(temp==a[i])
                count++;
            else
                count--;
        }
        count=0;
        for(int i=0;i<size;i++){
            if(temp==a[i])
                count++;
        }
        
        if(count>(size/2))
            return temp;
        return -1;
    }
    public static void main(String[] args) {
        int nums[]={2,2,1,1,1,2,2};
        System.out.println(majorityElement(nums));
        System.out.println(majorityElement_1(nums,nums.length));
    }
}