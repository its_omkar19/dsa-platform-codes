//GFG - Element with left side smaller and right side greater

class Compute {
    public static int findElement(int arr[], int n) {
        int gret[] = new int[n];
        int small[] = new int[n];
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            if (arr[i] > max)
                max = arr[i];
            gret[i] = max;

        }
        int min = Integer.MAX_VALUE;
        for (int i = n - 1; i >= 0; i--) {
            if (arr[i] < min)
                min = arr[i];
            small[i] = min;
        }
        for (int i = 0; i < n; i++) {
            if (i != 0 && i != n - 1) {
                if (gret[i] == small[i]) {
                    return small[i];
                }
            }
        }
        return -1;
    }

    public static void main(String args[]) {
        int arr[] = { 5, 6, 4, 1, 7, 12, 9, 1, 4, 1, 11, 5, 7, 1 };
        System.out.println(findElement(arr, 14));
    }
}