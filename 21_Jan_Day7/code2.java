// GFG - Wave Form

class Solution {
    public static void convertToWave(int n, int[] arr) {
        for(int i=0;i<n-1;i=i+2){
            arr[i]=arr[i]+arr[i+1];
            arr[i+1]=arr[i]-arr[i+1];
            arr[i]=arr[i]-arr[i+1];
        }
    }
    public static void main(String[] args) {
        int arr[]={1,2,3,4,5};
        convertToWave(arr.length, arr);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}