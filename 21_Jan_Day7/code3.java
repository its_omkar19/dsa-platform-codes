// GFG - Convert array into Zig-Zag fashion

class Solution{
    public static void zigZag(int a[], int n){
        for(int i=0;i<n-1;i++){
            if(i%2==0 && a[i]>a[i+1]){
                a[i]=a[i]+a[i+1];
                a[i+1]=a[i]-a[i+1];
                a[i]=a[i]-a[i+1];
            }
            else if(i%2==1 && a[i]<a[i+1]){
                a[i]=a[i]+a[i+1];
                a[i+1]=a[i]-a[i+1];
                a[i]=a[i]-a[i+1];
            }
        }
    }
    public static void main(String[] args) {
        int a[]={4, 3, 7, 8, 6, 2, 1};
        zigZag(a, a.length);
        for(int e:a){
            System.out.println(e);
        }
    }
}