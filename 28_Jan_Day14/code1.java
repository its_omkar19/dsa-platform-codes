// Leetcode 349 - Intersection of Two Arrays

import java.util.*;
class Solution {
    public static int[] intersection(int[] nums1, int[] nums2) {
        HashSet hs=new HashSet();
        for(int e:nums1)
            hs.add(e);
        ArrayList<Integer> inter=new ArrayList<>();
        for(int i=0;i<nums2.length;i++){
            if(hs.contains(nums2[i])){
                hs.remove(nums2[i]);
                inter.add(nums2[i]);
            }
        }
        int arr[]=new int[inter.size()];
        for(int i=0;i<arr.length;i++){
            arr[i]=inter.get(i);
        }
        return arr;
    }
    public static void main(String[] args) {
        int nums1[]={1,2,2,1};
        int nums2[]={2,2};
        int arr[]=intersection(nums1, nums2);
        for(int x:arr){
            System.out.println(x);
        }
    }   
}