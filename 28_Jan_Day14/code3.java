// GFG - Smallest subarray with sum greater than x

class Solution {

    public static int smallestSubWithSum(int a[], int n, int x) {
        
        int sum=0;
        int s=0;
        int len=0;
        int count=Integer.MAX_VALUE;
        for(int i=0;i<n;i++){
            sum=sum+a[i];
            if(sum>x){
                len = i - s + 1;
                if(len<count)
                    count=len;
                while( s < i && sum>x){
                    sum = sum - a[s];
                    s++;
                    if(sum > x){
                        len=i - s + 1;
                        if(len < count)
                            count=len;
                    }
                }
            }
        }
        if(count==Integer.MAX_VALUE)
            return 0;
        return count;
    }
    public static void main(String[] args) {
        int a[]={1, 10, 5, 2, 7};
        System.out.println(smallestSubWithSum(a, a.length, 9));
    }
}