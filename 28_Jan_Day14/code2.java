// GFG - Length Unsorted Subarray

class c2day14 {
    static int[] printUnsorted(int[] arr, int n) {
        int min=-1;
        int max=-1;
        int i=0;
        int j=n-1;
        for(i=0;i<n-1;i++){
            if(arr[i]>arr[i+1])
            break;
        }
        if(i==n-1)
            return new int[]{0,0};
        
        for(j=n-1;j>0;j--){
            if(arr[j]  < arr[j-1])
                break;
        }
        max=arr[i];
        min=arr[i];
        for(int p=i+1;p<=j;p++){
            if(arr[p]>max)
                max=arr[p];
            if(arr[p]<min)
                min=arr[p];
        }
        for(int p=0;p<i;p++){
            if(arr[p]>min){
                i=p;
                break;
            }
                
        }
        for(int p=n-1;p>=j+1;p--){
            if(arr[p]<max){
                j=p;
                break;
            }
        }
            
        return new int[]{i,j};
    }
    public static void main(String[] args) {
        int arr[]={10,12,20,30,25,40,32,31,35,50,60};
        int ar[]=printUnsorted(arr,arr.length);
        System.out.println(ar[0]);
        System.out.println(ar[1]);
    }

}