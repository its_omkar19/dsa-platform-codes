//Leetcode 1480 - Running Sum of 1d Array

class Solution {
    public static int[] runningSum(int[] nums) {
        int sum=0;
        for(int i=0;i<nums.length;i++){
            sum=sum+nums[i];
            nums[i]=sum;
        }
        return nums;
    }
    public static void main(String[] args) {
        int nums[]={1,1,1,1,1};
        nums=runningSum(nums);
        for(int e:nums)
            System.out.println(e);
    }
}