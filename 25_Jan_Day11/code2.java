//Leetcode 2022 - Convert 1D Array Into 2D Array

class Solution {
    public static int[][] construct2DArray(int[] original, int m, int n) {
        int arr[][]=new int[m][n];
        if(n*m!=original.length){
                return new int[][]{};
        }
        int k=0;
        int j=0;
        for(int i=0;i<original.length;i++){
            if(j==n){
                j=0;
                k++;
            }
            arr[k][j]=original[i];
            j++;
        }
        return arr;
    }
    public static void main(String[] args) {
        int original[]={1,2,3,4};
        int arr[][]=construct2DArray(original, 2, 2);
        for(int arr1[]:arr)
            for(int e:arr1)
            System.out.println(e);
    }
}