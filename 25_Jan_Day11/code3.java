//GFG - Array Subset of another array

import java.util.ArrayList;

class Compute {
    public static String isSubset( long a1[], long a2[], long n, long m) {
        ArrayList<Long> al=new ArrayList<>();
        for(int i=0;i<n;i++)
            al.add(a1[i]);
            
        for(int i=0;i<m;i++){
            if(!al.contains(a2[i]))
                return "No";
            al.remove(a2[i]);
        }
        return "Yes";        
    }
    public static void main(String[] args) {
        long a1[]={11, 7, 1, 13, 21, 3, 7, 3};
        long a2[]={11, 3, 7, 1, 7};
        System.out.println(isSubset(a1, a2, a1.length, a2.length));
    }
}