// Leetcode 121 - Best Time to Buy and Sell Stock

class Solution {
    public static int maxProfit(int[] prices) {
        int buy=Integer.MAX_VALUE;
        int mp=0;
        for(int i=0;i<prices.length;i++){
            if(prices[i]<buy){
                buy=prices[i];
            }
            if(prices[i]-buy>mp)
                mp=prices[i]-buy;
        }
        return mp;
    }
    public static void main(String[] args) {
        int prices[]={7,1,5,3,6,4};
        System.out.println(maxProfit(prices));
    }
}