// GFG - Bitonic Point

class Solution {
    static int findMaximum(int[] arr, int n) {
        for(int i=0;i<n-1;i++){
            if(arr[i]>arr[i+1])
                return arr[i];
        }
        
        return arr[n-1];
        
    }
    public static void main(String[] args) {
        int arr[]={1,15,25,45,42,21,17,12,11};
        System.out.println(findMaximum(arr,arr.length));
    }
}