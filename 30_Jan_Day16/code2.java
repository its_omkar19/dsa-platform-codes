// GFG - Binary Search 

class Solution {
     static int binarysearch(int arr[], int n, int k) {
        for(int i=0;i<n;i++){
            if(arr[i]==k)
                return i;
        }
        return -1;
    }
    public static void main(String[] args) {
        int arr[]={1,2,3,4,5};
        System.out.println(binarysearch(arr, arr.length, 4));
    }
}