import java.util.ArrayList;
import java.util.Collections;

class Solution
{
    public static long findMinDiff (ArrayList<Integer> a, int n, int m)
    {
        Collections.sort(a);
        int dif=Integer.MAX_VALUE;
        for(int i=0;i<n-m+1;i++){
            int j=i+m-1;
            if(dif > a.get(j)-a.get(i) )
                dif=a.get(j)-a.get(i);
        }
        return dif;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a=new ArrayList<>();
        a.add(3);
        a.add(4);
        a.add(1);
        a.add(1);
        a.add(9);
        a.add(56);
        a.add(7);
        a.add(9);
        a.add(12);
        System.out.println(findMinDiff(a,a.size(),5));
    }   
}