//Leetcode 628 & GFG -Maximum Product of Three Numbers


class Solution {
    public static int maximumProduct(int[] arr) {
        int m1 = Integer.MIN_VALUE;
        int m2 = Integer.MIN_VALUE;
        int m3 = Integer.MIN_VALUE;
        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;        
        
        for(int i=0;i<arr.length;i++){
            if(arr[i]>m1){
                m3=m2;
                m2=m1;
                m1=arr[i];
            }
            else if(arr[i]>m2){
                m3=m2;
                m2=arr[i];
            }
            else if(arr[i]>m3){
                m3=arr[i];
            }
            if(min1>arr[i]){
                min2=min1;
                min1=arr[i];
            }
            else if(min2>arr[i]){
                min2=arr[i];
            }
        }
        if(m1*m2*m3 <  m1*min1*min2)
            return m1*min1*min2;
        return m1*m2*m3;
    }
public static void main(String args[]){
    int arr[]={1,2,3,4};
    System.out.println(maximumProduct(arr));
}

}