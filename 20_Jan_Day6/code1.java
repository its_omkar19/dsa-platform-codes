//Leetcode 26 & GFG - Remove Duplicates from Sorted Array

class Solution {
    public static int removeDuplicates(int[] nums) {
        int j=0;
        int num=Integer.MAX_VALUE;
        for(int i=0;i<nums.length;i++){
            if(nums[i]!=num){
                nums[j]=nums[i];
                j++;
            }
            num=nums[i];
        }
        return j;
    }
    public static void main(String[] args) {
        int nums[]={0,0,1,1,1,2,2,3,3,4};
        System.out.println(removeDuplicates(nums));
    }
}