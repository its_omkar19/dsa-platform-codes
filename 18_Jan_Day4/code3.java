//GFG - Even and Odd
// Even element at even index and Odd element at odd index

class Solution {
    static void reArrange(int[] arr, int N) {
        int even=0;
        int odd=1;
        while(true){
            while(even<N && arr[even]%2==0)
                even = even+2;
                
            while(odd<N && arr[odd]%2==1)
                odd = odd+2;
            
            if(even<N && odd<N){
                arr[even]=arr[even]+arr[odd];
                arr[odd]=arr[even]-arr[odd];
                arr[even]=arr[even]-arr[odd];
            }
            else
                break;
        }
    }
    public static void main(String[] args) {
        int arr[]={3, 6, 12, 1, 5, 8};
        reArrange(arr,arr.length);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}