//Leetcode 485 Max Consecutive Ones
class Solution {
    public static int findMaxConsecutiveOnes(int[] nums) {
        int count=0;
        int count1=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==1)
                count1++;
            else{
                if(count1>count)
                    count=count1;
                    count1=0;
            }
        }
        if(count1>count)
            count=count1;
        if(count==0 )
            return count1;
        return count;
    }
    public static void main(String[] args) {
        int arr[]={1,1,0,1,1,1};
        System.out.println(findMaxConsecutiveOnes(arr));
    }
}