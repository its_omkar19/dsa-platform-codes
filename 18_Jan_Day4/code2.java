//Leetcode 496 - Next Greater Element I

import java.util.ArrayList;

class Solution {
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        ArrayList al=new ArrayList();
        for(int i=0;i<nums2.length;i++)
            al.add(nums2[i]);
        for(int i=0;i<nums1.length;i++){
            if(al.contains(nums1[i])){
                int flag=1;
                for(int j=al.indexOf(nums1[i]);j<nums2.length;j++){
                    if(nums2[j]>nums1[i]){
                        nums1[i]=nums2[j];
                        flag=0;
                        break;
                    }
                }
                if(flag==1)
                    nums1[i]=-1;
            }
        }
        return nums1;
    }
    public static void main(String[] args) {
        int nums1[]={4,1,2};
        int nums2[]={1,3,4,2};
        nextGreaterElement(nums1, nums2);
        for(int i:nums1)
            System.out.println(i);
    }
}