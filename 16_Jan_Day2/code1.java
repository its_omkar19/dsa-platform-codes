//Leetcode 1 - Two sum

import java.util.ArrayList;

class Solution {
    public static int[] twoSum(int[] nums, int target) {
        ArrayList<Integer> ar=new ArrayList<>();
        for(int i=0;i<nums.length;i++){
            ar.add(nums[i]);
        }
        for(int i=0;i<nums.length;i++){
            int rem =target-nums[i];
            if(ar.contains(rem)){
                if(ar.indexOf(rem)!=i)
                return new int[]{i,ar.indexOf(rem)};
            }
        }

        return new int[2];
    }
    public static void main(String[] args) {
        int nums[]=new int[]{3,2,2,3};
        int arr[]=twoSum(nums, 9);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}