//Leetcode 27 - Remove Element

class Solution {
    public static int removeElement(int[] nums, int val) {
        int count=0;
        for(int i=0;i<nums.length;i++){
            if(val!=nums[i]){
                nums[count]=nums[i];
                count++;
            }
        }
        return count;
    }
    public static void main(String[] args) {
        int nums[]=new int[]{3,2,2,3};
        System.out.println(removeElement(nums,3));
    }
}