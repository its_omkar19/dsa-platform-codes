//GFG - Find the smallest and second smallest element in an array

class Compute 
{
    public static long[] minAnd2ndMin(long a[], long n)  
    {
        Long fSmall=Long.MAX_VALUE;
        Long sSmall=Long.MAX_VALUE;
        for(int i=0;i<n;i++){
            if(fSmall>a[i])
                fSmall=a[i];
        }
        for(int i=0;i<n;i++){
            if(sSmall > a[i] && a[i]>fSmall)
                sSmall=a[i];
        }
        if(fSmall != sSmall && sSmall==Long.MAX_VALUE)
            return new long[]{-1,-1};
        if( fSmall==sSmall)
            return new long[]{-1,-1};
        return new long[]{fSmall,sSmall};
        
    }
    public static void main(String[] args) {
        long nums[]=new long[]{2,4,3,5,6};
        long arr[]=minAnd2ndMin(nums, nums.length);
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
    }
}