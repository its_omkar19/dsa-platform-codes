// Leetcode 66 - Plus one

class Solution {
    public static int[] plusOne(int[] digits) {
        if(digits[digits.length-1]!=9){
            digits[digits.length-1]++;
            return digits;
        }
        int flag=0;
        for(int i=0;i<digits.length;i++){
            if(digits[i]!=9)
                flag=1;
        }
        if(flag==0){
            int arr[]=new int[digits.length+1];
            for(int i=0;i<arr.length;i++){
                if(i==0)
                    arr[i]=1;
                else
                    arr[i]=0;
            }
            return arr;
        }
        for(int i=digits.length-1;i>=0;i--){
            if( digits[digits.length-1]==9 || (digits[i]>9 && i!=0)){
                digits[i]=0;
                digits[i-1]++;
            }
            
        }
        return digits;
    }
    public static void main(String[] args) {
        int digits[]={1,2,3};
        int a[]=plusOne(digits);
        for(int x:a){
            System.out.println(x);
        }
    }
}