// Leetcode 1995 - Count Special Quadruplets

import java.util.HashMap;

class Solution {
    public static int countQuadruplets(int[] nums) {
        int ret=0;
        HashMap<Integer,Integer> hm=new HashMap<>();
        for(int i=nums.length-2;i>=1;i--){
            for(int j=i+1;j<nums.length;j++){
                int num=nums[j]-nums[i];
                hm.put(num,hm.getOrDefault(num,0)+1);
            }
            for(int j=0;j<i-1;j++){
                int num=nums[j]+nums[i-1];
                ret += hm.getOrDefault(num,0);
            }
        }
        return ret;
    }
    public static void main(String[] args) {
        int nums[]={1,2,3,6};
        System.out.println(countQuadruplets(nums));
    }
}