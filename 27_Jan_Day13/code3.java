// GFG - Minimum distance between two numbers

class Solution {
    static int minDist(int a[], int n, int x, int y) {
        int last=-1;
        int ret=Integer.MAX_VALUE;
        for(int i=0;i<n;i++){
            if(x==a[i] || y==a[i]){
                if(last!=-1 && a[last]!=a[i]){
                    ret= Math.min(ret,i-last);
                }
                last=i;
            }
        }
        return ret==Integer.MAX_VALUE?-1:ret;
    }
    public static void main(String[] args) {
        int a[]={1,2,3,4};
        System.out.println(minDist(a, a.length, 1, 2));
    }
}