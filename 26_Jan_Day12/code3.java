// GFG - Left most and right most index

class Solution {
    
    public static void indexes(long v[], long x)
    {
        int first=-1;
        int last=-1;
        for(int i=0;i<v.length;i++){
            if(v[i]==x && first==-1){
                first=i;
            }
            if(v[i]==x && first != -1){
                last=i;
            }
        }
        System.out.println(first);
        System.out.println(last);
    }
    public static void main(String[] args) {
        long v[]={1, 3, 5, 5, 5, 5, 67, 123, 125};
        indexes(v, 5);
    }
}